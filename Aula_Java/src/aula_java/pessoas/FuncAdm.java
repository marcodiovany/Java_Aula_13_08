/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula_java.pessoas;

import aula_java.Conexao.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class FuncAdm extends Funcionario{
    private String setor;

    public FuncAdm(String setor, String funcao) {
        this.setor = setor;
        this.funcao = funcao;
    }

    public FuncAdm() {
    }
    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }
    @Override
    public String getFuncao() {
        return super.funcao;
    }
    public void salvar()
    {
        super.salvar();
        Conexao c = new Conexao();
        Connection conexao = c.getConexao();
        
        PreparedStatement ps;
        
        try
        {
            ps = conexao.prepareStatement("Insert into OO_HERANCA_FUNC (nome,setor,func) values (?,?,?)");
            ps.setString(1, this.nome);
            ps.setString(2, this.setor);
            ps.setString(3, this.funcao);
            ps.executeUpdate();
            
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void atualizar()
    {
        super.atulizar(this.setor,this.funcao,this);
    }
}
