/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula_java.pessoas;
import aula_java.Conexao.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author Aluno
 */
public abstract class Pessoa {
    protected String nome,endereco,funcao;
    protected int idade;
    
    public abstract String getFuncao();
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
    
    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }
    public void salvar()
    {
        Conexao c = new Conexao();
        Connection conexao = c.getConexao();
        
        PreparedStatement ps;
        
        try
        {
            ps = conexao.prepareStatement("Insert into OO_HERANCA_PESSOA3 (nome,idade,endereco) values (?,?,?)");
            ps.setString(1, this.nome);
            ps.setInt(2, this.idade);
            ps.setString(3, this.endereco);
            ps.executeUpdate();
            
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void salvar(String atributo1,String atributo2,Pessoa p)
    {
        Conexao c = new Conexao();
        Connection conexao = c.getConexao();
        
        PreparedStatement ps;
        try
        {
            ps = conexao.prepareStatement("Insert into OO_HERANCA_PESSOA (nome,idade,endereco,atributo1,atributo2,tipo) values (?,?,?,?,?,?)");
            ps.setString(1, this.nome);
            ps.setInt(2, this.idade);
            ps.setString(3, this.endereco);
            ps.setString(4, atributo1);
            ps.setString(5, atributo2);
            if(p instanceof Aluno)
            {
                ps.setString(6, "Aluno");
            }
            if(p instanceof Professor)
            {
                ps.setString(6, "Professor");
            }
            if(p instanceof FuncAdm)
            {
                ps.setString(6, "Func");
            }
            ps.executeUpdate();
            
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void atualizar()
    {
                Conexao c = new Conexao();
        Connection conexao = c.getConexao();
        
        PreparedStatement ps;
        try
        {
            ps = conexao.prepareStatement("UPDATE OO_HERANCA_PESSOA set idade = ?,endereco = ? where nome = ?");
            ps.setString(3, this.nome);
            ps.setInt(1, this.idade);
            ps.setString(2, this.endereco);
            ps.executeUpdate();
            
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
        
    }
    public void atulizar(String atributo1,String atributo2,Pessoa p)
    {
         Conexao c = new Conexao();
        Connection conexao = c.getConexao();
        
        PreparedStatement ps;
        try
        {
            ps = conexao.prepareStatement("UPDATE OO_HERANCA_PESSOA set idade = ?,endereco = ?,atributo1=?,atributo2=?,tipo=? where nome = ?");
            ps.setString(6, this.nome);
            ps.setInt(1, this.idade);
            ps.setString(2, this.endereco);
            ps.setString(3, atributo1);
            ps.setString(4, atributo2);
            if(p instanceof Aluno)
            {
                ps.setString(5, "Aluno");
            }
            if(p instanceof Professor)
            {
                ps.setString(5, "Professor");
            }
            if(p instanceof FuncAdm)
            {
                ps.setString(5, "Func");
            }
            ps.executeUpdate();
            
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void deletar()
    {
        Conexao c = new Conexao();
        Connection conexao = c.getConexao();
        
        PreparedStatement ps;
        try
        {
            ps = conexao.prepareStatement("DELETE FROM OO_HERANCA_PESSOA WHERE nome = ?");
            ps.setString(1, this.nome);
            ps.executeUpdate();
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
        
    }
    public static ArrayList<Pessoa> getAll()
    {
        Conexao c = new Conexao();
        Connection conexao = c.getConexao();
        ArrayList<Pessoa> pessoas = new ArrayList<>();
        PreparedStatement ps;
        try
        {
            ps = conexao.prepareStatement("SELECT * FROM OO_HERANCA_PESSOA");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                String tipo = rs.getString("tipo");
                Pessoa nova =new Aluno();
                if(tipo.equals("Aluno"))
                {
                    nova = new Aluno();
                    ((Aluno)nova).setSemestre(rs.getString("atributo1"));
                    ((Aluno)nova).setCurso(rs.getString("atributo2"));
                }else
                if(tipo.equals("Professor"))
                {
                    nova = new Professor();
                    ((Professor)nova).setDisciplina(rs.getString("atributo1"));
                }else
                if(tipo.equals("Func"))
                {
                    nova = new FuncAdm();
                    ((FuncAdm)nova).setSetor(rs.getString("atributo1"));
                    ((FuncAdm)nova).setFuncao(rs.getString("atributo2"));
                }
                nova.setNome(rs.getString("nome"));
                nova.setIdade(rs.getInt("idade"));
                nova.setEndereco(rs.getString("endereco"));
                pessoas.add(nova);
            }
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
        return pessoas;
    }
    @Override
    public String toString() {
        return "Pessoa{" + "nome=" + nome + ", endereco=" + endereco + ", funcao=" + funcao + ", idade=" + idade + '}';
    }
}
