/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula_java.pessoas;

import aula_java.Conexao.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Aluno extends Pessoa{
    private String curso,semestre;

    public Aluno(String curso, String semestre) {
        this.funcao = "Aluno";
        this.curso = curso;
        this.semestre = semestre;
    }

    public Aluno() {
        this.funcao = "Aluno";
    }
    
    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    @Override
    public String getFuncao() {
        return funcao;
    }
    public void salvar()
    {
        super.salvar();
        Conexao c = new Conexao();
        Connection conexao = c.getConexao();
        
        PreparedStatement ps;
        
        try
        {
            ps = conexao.prepareStatement("Insert into OO_HERANCA_ALUNO (nome,semestre,curso) values (?,?,?)");
            ps.setString(1, this.nome);
            ps.setString(2, this.semestre);
            ps.setString(3, this.curso);
            ps.executeUpdate();
            
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void atualizar()
    {
        super.atulizar(this.semestre,this.curso,this);
    }
}
