/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula_java.pessoas;

import aula_java.Conexao.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


/**
 *
 * @author Aluno
 */
public class Professor extends Funcionario{
    private String disciplina;

    public Professor(String disciplina) {
        this.funcao = "Professor";
        this.disciplina = disciplina;
    }

    public Professor() {
        this.funcao = "Professor";
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }
    @Override
    public String getFuncao() {
        return funcao;
    }
    public void salvar()
    {
        super.salvar();
        Conexao c = new Conexao();
        Connection conexao = c.getConexao();
        
        PreparedStatement ps;
        
        try
        {
            ps = conexao.prepareStatement("Insert into OO_HERANCA_PROFESSOR (nome,disciplina) values (?,?)");
            ps.setString(1, this.nome);
            ps.setString(2, this.disciplina);
            ps.executeUpdate();
            
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void atualizar()
    {
        super.atulizar(this.disciplina,"",this);
    }
}
