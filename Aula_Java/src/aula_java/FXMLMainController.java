/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula_java;

import aula_java.pessoas.Aluno;
import aula_java.pessoas.Pessoa;
import aula_java.pessoas.Professor;
import aula_java.pessoas.FuncAdm;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Marco Diovany Maronez Alves
 */
public class FXMLMainController implements Initializable {
    
    @FXML
    private TableView<Pessoa> TablePessoa;
    @FXML
    private TableColumn<Pessoa, ?> ColNome;
    @FXML
    private TableColumn<Pessoa, ?> ColIdade;
    @FXML
    private TableColumn<Pessoa, ?> ColEndereco;
    @FXML
    private TableColumn<Pessoa, ?> ColFuncao;
    @FXML 
    private AnchorPane formDados;
    @FXML
    private Label lblAtributo1;
    @FXML
    private Label lblTipo;
    @FXML
    private Label lblAtributo2;
    @FXML
    private TextField tbxNome;
    @FXML
    private TextField tbxIdade;
    @FXML
    private TextField tbxEndereco;
    @FXML
    private TextField tbxAtributo2;
    @FXML
    private TextField tbxAtributo1;
    private Pessoa pessoa=null;
    @FXML
    private Button btnAtualizar;
    @FXML
    private Button btnDeletar;
    @FXML
    private RadioButton rbnFunc;
    @FXML
    private RadioButton rbnProf;
    @FXML
    private RadioButton rbnAluno;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        TablePessoa.getItems().clear();
        ColNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        ColIdade.setCellValueFactory(new PropertyValueFactory<>("idade"));
        ColEndereco.setCellValueFactory(new PropertyValueFactory<>("endereco"));
        ColFuncao.setCellValueFactory(new PropertyValueFactory<>("funcao"));
        setTypeForm(new FuncAdm());
        TablePessoa.getItems().addAll(Pessoa.getAll());
    }    

    private void cadastrarNovo(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLCadastrar.fxml"));
        Scene scene = new Scene(root);
        Aula_Java.tela.setScene(scene);
    }
    void setTypeForm(Pessoa p)
    {
        if(p instanceof Aluno)
        {
            togleRbn(3);
            setVisibleLvl(true, 2);
            lblTipo.setText("Aluno");
            lblAtributo1.setText("Semestre: ");
            lblAtributo2.setText("Curso: ");
        }
        if(p instanceof Professor)
        {
            togleRbn(2);
            setVisibleLvl(true, 1);
            lblTipo.setText("Professor");
            lblAtributo1.setText("Disciplina: ");
        }
        if(p instanceof FuncAdm)
        {
            togleRbn(1);
            setVisibleLvl(true, 2);
            lblTipo.setText("Funcionario");
            lblAtributo1.setText("Setor: ");
            lblAtributo2.setText("Funcao: ");
        }
        tbxNome.clear();
        tbxIdade.clear();
        tbxEndereco.clear();
        tbxAtributo1.clear();
        tbxAtributo2.clear();
    }
    private void setVisibleLvl(boolean visible,int lvl)
    {
        formDados.setVisible(visible);
        if(lvl > 1)
        {
            lblAtributo1.setVisible(visible);
            tbxAtributo1.setVisible(visible);
            lblAtributo2.setVisible(visible);
            tbxAtributo2.setVisible(visible);
            
        }else if(lvl > 0)
        {
            lblAtributo1.setVisible(visible);
            tbxAtributo1.setVisible(visible);
            lblAtributo2.setVisible(false);
            tbxAtributo2.setVisible(false);
        }
    }

    @FXML
    private void getSelecao(MouseEvent event) {
        pessoa = TablePessoa.getSelectionModel().getSelectedItem();
        setTypeForm(pessoa);
        if(pessoa instanceof Aluno)
        {
            setTypeForm(pessoa);
            tbxAtributo1.setText(((Aluno)pessoa).getSemestre());
            tbxAtributo2.setText(((Aluno)pessoa).getCurso());
        }
        if(pessoa instanceof Professor)
        {
            setTypeForm(pessoa);
            tbxAtributo1.setText(((Professor)pessoa).getDisciplina());

        }
        if(pessoa instanceof FuncAdm)
        {
            setTypeForm(pessoa);
            tbxAtributo1.setText(((FuncAdm)pessoa).getSetor());
            tbxAtributo2.setText(((FuncAdm)pessoa).getFuncao());
        }
        tbxNome.setText(pessoa.getNome());
        tbxIdade.setText(String.valueOf(pessoa.getIdade()));
        tbxEndereco.setText(pessoa.getEndereco());
        lblTipo.setText(pessoa.getFuncao());
    }

    @FXML
    private void atualizarPessoa(ActionEvent event) throws Exception {
        boolean existe=true;
        if(pessoa == null)
        {
            if(rbnFunc.isSelected())
            {
                pessoa = new FuncAdm();
            }else if(rbnProf.isSelected())
            {
                pessoa = new Professor();
            }else if(rbnAluno.isSelected())
            {
                pessoa = new Aluno();
            }else
            {
                throw new Exception("Selecione um tipo");
            }
            TablePessoa.getItems().add(pessoa);
            existe=false;
        }
            pessoa.setNome(tbxNome.getText());
            pessoa.setIdade(Integer.valueOf(tbxIdade.getText()));
            pessoa.setEndereco(tbxEndereco.getText());
            if(pessoa instanceof FuncAdm)
            {
                ((FuncAdm)pessoa).setSetor(tbxAtributo1.getText());
                pessoa.setFuncao(tbxAtributo2.getText());
                if(existe)
                {
                    ((FuncAdm)pessoa).atualizar();
                }else
                {
                    ((FuncAdm)pessoa).salvar();
                }
            }
            if(pessoa instanceof Professor)
            {
                ((Professor)pessoa).setDisciplina(tbxAtributo1.getText());
                if(existe)
                {
                    ((Professor)pessoa).atualizar();
                }else
                {
                    ((Professor)pessoa).salvar();
                }
            }
            if(pessoa instanceof Aluno)
            {
                ((Aluno)pessoa).setSemestre(tbxAtributo1.getText());
                ((Aluno)pessoa).setCurso(tbxAtributo2.getText());
                
                if(existe)
                {
                    ((Aluno)pessoa).atualizar();
                }else
                {
                    ((Aluno)pessoa).salvar();
                }
            }
            System.out.println(pessoa.toString());
            pessoa = null;
            TablePessoa.refresh();
    }

    @FXML
    private void deletarPessoa(ActionEvent event) {
        pessoa.deletar();
        TablePessoa.getItems().remove(pessoa);
        TablePessoa.refresh();
        pessoa = null;
    }
    void togleRbn(int i)
    {
        if(i == 1)
        {
            rbnFunc.setSelected(true);
            rbnProf.setSelected(false);
            rbnAluno.setSelected(false);
        }else if(i == 2)
        {
            rbnFunc.setSelected(false);
            rbnProf.setSelected(true);
            rbnAluno.setSelected(false);
        }else if(i==3)
        {
            rbnFunc.setSelected(false);
            rbnProf.setSelected(false);
            rbnAluno.setSelected(true);
        }else
        {
            rbnFunc.setSelected(false);
            rbnProf.setSelected(false);
            rbnAluno.setSelected(false);
        }
    }
    @FXML
    private void rbnFuncClick(ActionEvent event) {
        setTypeForm(new FuncAdm());
    }

    @FXML
    private void rbnProfClick(ActionEvent event) {
        setTypeForm(new Professor());
    }

    @FXML
    private void rbnAlunoClick(ActionEvent event) {
        setTypeForm(new Aluno());
    }

    @FXML
    private void limparCampo(ActionEvent event) {
        tbxNome.clear();
        tbxIdade.clear();
        tbxEndereco.clear();
        tbxAtributo1.clear();
        tbxAtributo2.clear();
        pessoa = null;
        TablePessoa.refresh();
    }
    
}
